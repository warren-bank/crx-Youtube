### [Youtube](https://github.com/warren-bank/crx-Youtube/tree/webmonkey-userscript/es6)

[Userscript](https://github.com/warren-bank/crx-Youtube/raw/webmonkey-userscript/es6/webmonkey-userscript/Youtube.user.js) to run in:
* the [WebMonkey](https://github.com/warren-bank/Android-WebMonkey) application
  - for Android
* the [Tampermonkey](https://www.tampermonkey.net/) web browser extension
  - for [Firefox/Fenix](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
  - for [Chrome/Chromium](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
* the [Violentmonkey](https://violentmonkey.github.io/) web browser extension
  - for [Firefox/Fenix](https://addons.mozilla.org/firefox/addon/violentmonkey/)
  - for [Chrome/Chromium](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag)

Its purpose is to:
* on a [youtube.com](https://youtube.com/) page for a video:
  - add a button to toggle the display of a list of all available media formats
  - for each available media format, include:
    * a brief summary of its attributes
    * _Start Media_ button to transfer the chosen media to an external player
    * _Show Details_ button to expand a block of hidden text that contains all available technical details about the media format
    * a grouping of icons to transfer the chosen media to various tools on the [Webcast-Reloaded](https://github.com/warren-bank/crx-webcast-reloaded) external [website](https://warren-bank.github.io/crx-webcast-reloaded/external_website/index.html)
      - mainly for use with:
        * _Google Chromecast_
        * [_ExoAirPlayer_](https://github.com/warren-bank/Android-ExoPlayer-AirPlay-Receiver)
        * [_HLS-Proxy_](https://github.com/warren-bank/HLS-Proxy)

#### Important:

* does not currently work when the `User-Agent` request header self-identifies as a mobile device
  - which causes _Youtube_ to redirect from `www.youtube.com` to `m.youtube.com`
  - in _Fenix_:
    * open: main menu
    * select: "Desktop site"
  - in _WebMonkey_:
    * open: main menu &gt; _Settings_ &gt; _User Agent_
    * select either: "Chrome 120, Windows 10", or "Custom User Agent"

#### Legal:

* copyright: [Warren Bank](https://github.com/warren-bank)
* license: [GPL-2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)
